import React from "react";

function ToDoItem(props) {
  function handleClick() {}

  return (
    <div>
      <li>
        <input
          type="checkbox"
          onClick={() => {
            props.onChecked(props.id);
          }}
        ></input>
        {props.text}
      </li>
    </div>
  );
}

export default ToDoItem;
